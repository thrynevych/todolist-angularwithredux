export interface ITodo {
  id: number;
  description: string;
  responsible: string;
  prioriyty: string;
  isCompleted: boolean;
}
