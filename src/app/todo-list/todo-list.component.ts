import { Component, OnInit } from '@angular/core';
import {NgRedux, select, dispatch} from '@angular-redux/store';
import { IAppState } from '../store';
import { ADD_TODO, REMOVE_TODO, TOGGLE_TODO } from '../actions';
import { ITodo } from '../todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  @select() todos;

  model: ITodo = {
    id: 0,
    description: '',
    responsible: '',
    prioriyty: 'low',
    isCompleted: false
  };

  constructor(private ngRegux: NgRedux) { }

  ngOnInit() {
  }

  onSubmit() {
    this.ngRegux.dispatch({type: ADD_TODO, todo: this.model})
  }

  toggleTodo(todo) {
    this.ngRegux.dispatch({type: TOGGLE_TODO, id: todo.id});
  }

  removeTodo(todo) {
    this.ngRegux.dispatch({type: REMOVE_TODO, id: todo.id});
  }

}
